FROM node:alpine

ADD . /app/

WORKDIR /app

RUN npm install && env

EXPOSE 3000

CMD npm start