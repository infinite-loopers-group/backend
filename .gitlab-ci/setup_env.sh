#!/bin/bash

# Récupérer les valeurs des variables d'environnement depuis GitLab
DB_CONNECTION_STRING=$1
DB_NEW_API_KEY=$2

# Configurer les variables d'environnement sur la machine de production
export MONGO_CONNECTION_STRING=$DB_CONNECTION_STRING
export MONGO_NEWS_API_KEY=$DB_NEW_API_KEY
