from locust import HttpUser, between, task

class MyUser(HttpUser):
    wait_time = between(5, 9)  # Temps d'attente entre les tâches, en secondes

    @task
    def fetch_articles(self):
        response = self.client.get("/articles")
        if response.status_code == 200:
            print("Articles fetched successfully")
        else:
            print("Failed to fetch articles")

    @task
    def signup_and_signin(self):
        # Signup
        response = self.client.post("/signup", json={"username": "root", "password": "root"})
        if response.status_code == 200:
            print("User signed up successfully")
        else:
            print("Failed to sign up user")

        # Signin
        response = self.client.post("/signin", json={"username": "root", "password": "root"})
        if response.status_code == 200:
            print("User signed in successfully")
        else:
            print("Failed to sign in user")
